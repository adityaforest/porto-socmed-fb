const { Comments } = require('../models')

module.exports = {
    create: async (req, res) => {
        try {
            await Comments.create({
                userId: req.body.userId,
                postId: req.body.postId,
                content: req.body.content,
                createdAt: new Date(),
                updatedAt: new Date()
            })
        } catch (e) {
            console.log(`error : CREATE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    },
    findByUserId: async (req, res) => {
        let userId = parseInt(req.params.userid)
        let result = {}
        try {
            result = await Comments.findAll({
                where: {
                    userId: userId
                }
            })
        } catch (e) {
            console.log(`error : FIND ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    findByPostId: async (req, res) => {
        let postId = parseInt(req.params.postid)
        let result = {}
        try {
            result = await Comments.findAll({
                where: {
                    postId: postId
                }
            })
        } catch (e) {
            console.log(`error : FIND ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    delete: async (req, res) => {
        let id = parseInt(req.params.id)
        try {
            await Comments.destroy({
                where: { id: id }
            })
        } catch (e) {
            console.log(`error : DELETE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    },
    deleteByPostId: async (req, res) => {
        let postid = parseInt(req.params.id)
        try {
            this.services.deleteByPostId(postid)
        } catch (e) {
            console.log(`error : DELETE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    },
    services: {
        deleteByPostId: async (postid) => {
            await Comments.destroy({
                where: { postId: postid }
            })
        }
    }
}