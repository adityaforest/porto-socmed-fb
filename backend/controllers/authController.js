const { Users } = require('../models')
const userCon = require('./userController')
const crypt = require('../lib/crypt')
const jwt = require('jsonwebtoken')


module.exports = {
    login: async (req, res) => {
        let response = {}
        try {
            let user = await Users.findOne({
                where: {
                    username: req.body.username
                }
            })

            if (!user) return res.status(401).json({ success: false, message: 'invalid username' })
            if (!crypt.compare(req.body.password, user.password)) return res.status(401).json({ success: false, message: 'invalid password' })

            response.id = user.id
            response.username = user.username
            response.dob = user.dob
            response.gender = user.gender
            response.accessToken = jwt.sign(response, 's3cr3tk3y')
            response.success = true

            res.json(response)
        } catch (e) {
            res.status(401)
            response.success = false
            response.message = e.message
        } finally {
            // res.json(response)
        }
    },
    register: userCon.create,
    verify: (req, res, next) => {
        try {
            let token = req.headers['authorization']

            if (token) {
                token = token.split(' ')[1]
                jwt.verify(token, 's3cr3tk3y')
            } else {
                return res.json({ success: false, message: 'token not provided' })
            }

        } catch (e) {
            if (e instanceof jwt.JsonWebTokenError) {
                return res.json({ success: false, message: 'invalid token' })
            }
        }

        next()
    },
    isUsernameAlreadyExist: async (req, res) => {
        let response = { usernameExist: false, success: true }
        try {
            let user = await Users.findOne({
                where: {
                    username: req.body.username
                }
            })
            if (user) {
                response.usernameExist = true
            }

        } catch (e) {
            res.status(401)
            response.success = false
            response.message = e.message
        }
        finally {
            res.json(response)
        }
    },
    getAllUsername: async (req, res) => {
        let result = []
        try {
            result = await Users.findAll({
                attributes: ['id','username']
            })
        } catch (e) {
            console.log(`error : FINDALL ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    }
}