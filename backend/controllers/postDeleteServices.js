const { Posts } = require('../models')

module.exports = {
    deleteByPostId: async (id) => {
        await Posts.destroy({
            where: { id: id }
        })

    }
}