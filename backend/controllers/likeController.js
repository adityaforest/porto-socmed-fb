const { Likes } = require('../models')

module.exports = {
    create: async (req, res) => {
        try {
            await Likes.create({
                userId: req.body.userId,
                postId: req.body.postId,
                createdAt: new Date(),
                updatedAt: new Date()
            })
        } catch (e) {
            console.log(`error : CREATE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    },
    findByUserId: async (req, res) => {
        let userId = parseInt(req.params.userid)
        let result = {}
        try {
            result = await Likes.findAll({
                where: {
                    userId: userId
                }
            })
        } catch (e) {
            console.log(`error : FIND ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    findByPostId: async (req, res) => {
        let postId = parseInt(req.params.postid)
        let result = {}
        try {
            result = await Likes.findAll({
                where: {
                    postId: postId
                }
            })
        } catch (e) {
            console.log(`error : FIND ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    findByUserAndPostId: async (req, res) => {
        let userId = parseInt(req.query.userid)
        let postId = parseInt(req.query.postid)
        let result = {}
        try {
            result = await Likes.findOne({
                where: {
                    userId: userId,
                    postId: postId
                }
            })
        } catch (e) {
            console.log(`error : FIND ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    deleteByUserAndPostId: async (req, res) => {
        let userId = parseInt(req.query.userid)
        let postId = parseInt(req.query.postid)
        try {
            await Likes.destroy({
                where: { userId: userId, postId: postId }
            })
        } catch (e) {
            console.log(`error : DELETE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    },
    deleteByPostId: async (req, res) => {
        let postid = parseInt(req.params.id)
        try {
            this.services.deleteByPostId(postid)
        } catch (e) {
            console.log(`error : DELETE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    },
    services: {
        deleteByPostId: async (postid) => {
            await Likes.destroy({
                where: { postId: postid }
            })
        }
    }
}