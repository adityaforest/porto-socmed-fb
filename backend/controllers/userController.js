const { Users } = require('../models')
const crypt = require('../lib/crypt')

module.exports = {
    create: async (req, res) => {
        try {
            await Users.create({
                username: req.body.username,
                password: crypt.encrypt(req.body.password),
                gender: req.body.gender,
                dob: req.body.dob,
                nationality: req.body.nationality,
                createdAt: new Date(),
                updatedAt: new Date()
            })
        } catch (e) {
            console.log(`error : CREATE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    },
    findAll: async (req, res) => {
        let result = {}
        try {
            result = await Users.findAll()
        } catch (e) {
            console.log(`error : FINDALL ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    find: async (req, res) => {
        let id = parseInt(req.params.id)
        let result = {}
        try {
            result = await Users.findOne({
                where: {
                    id: id
                },
                attributes:{exclude:['password']}
            })
        } catch (e) {
            console.log(`error : FIND ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    update: async (req, res) => {
        let id = parseInt(req.params.id)
        try {
            if (id == 1) return res.json({ success: false, message: 'cannot update admin account' })

            await Users.update({
                username: req.body.username,
                password: crypt.encrypt(req.body.password),
                gender: req.body.gender,
                dob: req.body.dob,
                nationality: req.body.nationality
            }, {
                where: {
                    id: id
                }
            })
        } catch (e) {
            console.log(`error : UPDATE ERROR ${e.message}`)
        }
        res.json({ success: true })
    },
    delete: async (req, res) => {
        let id = parseInt(req.params.id)
        try {
            if (id == 1) return res.json({ success: false, message: 'cannot delete admin account' })

            await Users.destroy({
                where: { id: id }
            })
        } catch (e) {
            console.log(`error : DELETE ERROR ${e.message}`)
        }
        res.json({ success: true })
    }
}