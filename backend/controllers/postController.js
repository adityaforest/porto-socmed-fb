const { Posts } = require('../models')
const LikeCon = require('./likeController')
const CommentCon = require('./commentController')
const PostDelCon = require('./postDeleteServices')

module.exports = {   
    create: async (req, res) => {
        try {
            await Posts.create({
                content: req.body.content,
                userId: req.body.userId,
                createdAt: new Date(),
                updatedAt: new Date()
            })
        } catch (e) {
            console.log(`error : CREATE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    },
    findAll: async (req, res) => {
        let result = {}
        try {
            result = await Posts.findAll()
        } catch (e) {
            console.log(`error : FINDALL ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    find: async (req, res) => {
        let id = parseInt(req.params.id)
        let result = {}
        try {
            result = await Posts.findOne({
                where: {
                    id: id
                }
            })
        } catch (e) {
            console.log(`error : FIND ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    findByUserId:async (req, res) => {
        let userId = parseInt(req.params.userid)
        let result = {}
        try {
            result = await Posts.findAll({
                where: {
                    userId: userId
                }
            })
        } catch (e) {
            console.log(`error : FIND ERROR ${e.message}`)
        }
        finally {
            res.json(result)
        }
    },
    update: async (req, res) => {
        let id = parseInt(req.params.id)
        try {
            await Posts.update({
                content: req.body.content,
                userId: req.body.userId,
            }, {
                where: {
                    id: id
                }
            })
        } catch (e) {
            console.log(`error : UPDATE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    },    
    delete: (req, res) => {
        let id = parseInt(req.params.id)
        try {
            PostDelCon.deleteByPostId(id)
            LikeCon.services.deleteByPostId(id)
            CommentCon.services.deleteByPostId(id)
        } catch (e) {
            console.log(`error : DELETE ERROR ${e.message}`)
        }
        finally {
            res.json({ success: true })
        }
    }
    
}