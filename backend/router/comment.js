const router = require('express').Router()
const comment = require('../controllers/commentController')

router.post('/' , comment.create)
router.get('/post/:postid' , comment.findByPostId)
router.get('/user/:userid' , comment.findByUserId)
router.delete('/:id' , comment.delete)

module.exports = router