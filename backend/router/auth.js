const router = require('express').Router()
const auth = require('../controllers/authController')

router.post('/login' , auth.login)
router.post('/register',auth.register)
router.post('/usernameExist' , auth.isUsernameAlreadyExist)
router.get('/usernameAll' , auth.getAllUsername)

module.exports = router