const router = require('express').Router()
const user = require('./user')
const post = require('./post')
const like = require('./like')
const comment = require('./comment')
const auth = require('./auth')

const secured = require('../controllers/authController').verify

//open
router.use('/api/v1/auth',auth)

//secure api below
router.use(secured)
router.use('/api/v1/users',user)
router.use('/api/v1/posts',post)
router.use('/api/v1/likes',like)
router.use('/api/v1/comments',comment)

module.exports = router