const router = require('express').Router()
const like = require('../controllers/likeController')

router.post('/' , like.create)
router.get('/post/:postid' , like.findByPostId)
router.get('/user/:userid' , like.findByUserId)
router.get('/' , like.findByUserAndPostId) //?userid=x&postid=x
router.delete('/' , like.deleteByUserAndPostId) //?userid=x&postid=x

module.exports = router