const router = require('express').Router()
const post = require('../controllers/postController')

router.post('/' , post.create)
router.get('/' , post.findAll)
router.get('/user/:userid' , post.findByUserId)
router.get('/:id' , post.find)
router.put('/:id' , post.update)
router.delete('/:id' , post.delete)

module.exports = router