const express = require('express')
const app = express()
const {PORT = 8080} = process.env

const cors = require('cors')
app.use(cors())

// app.use(express.urlencoded({extended:false}))
app.use(express.json())

const router = require('./router')
app.use(router)

app.listen(PORT, () => console.log(`server running on port ${PORT}`))