import React, { Component, useState } from 'react'
import { logout } from "../../actions/auth";
import { connect } from "react-redux";
import './Header.css'
import SearchIcon from '@mui/icons-material/Search'
import HomeIcon from '@mui/icons-material/Home'
import SmartDisplayIcon from '@mui/icons-material/SmartDisplayOutlined'
import StoreIcon from '@mui/icons-material/StorefrontOutlined'
import GroupIcon from '@mui/icons-material/GroupsRounded'
import ViewComfyIcon from '@mui/icons-material/ViewComfyOutlined'
import AppsIcon from '@mui/icons-material/Apps'
import SmsIcon from '@mui/icons-material/Sms'
import NotifIcon from '@mui/icons-material/Notifications'
import AccountIcon from '@mui/icons-material/AccountCircle'
import MenuIcon from '@mui/icons-material/Menu'
import PlusIcon from '@mui/icons-material/Add'
import { Avatar } from '@mui/material'
import SettingIcon from '@mui/icons-material/Settings'
import QuestionIcon from '@mui/icons-material/QuestionMarkRounded'
import MoonIcon from '@mui/icons-material/ModeNight'
import ExcIcon from '@mui/icons-material/Warning'
import ExitIcon from '@mui/icons-material/Logout'
import Logout from '@mui/icons-material/Logout'


class Header extends Component {
  constructor(props) {
    super(props)
    this.handleClickAccount = this.handleClickAccount.bind(this)
    this.logOut = this.logOut.bind(this);

    this.state = {
      accountDropdown: false
    }
  }

  handleClickAccount = e => {
    this.setState({
      accountDropdown: !this.state.accountDropdown
    })
  }

  logOut() {
    this.props.dispatch(logout());
  }

  render() {
    const { user: currentUser } = this.props;

    return (
      <div className='header'>
        <div className="header-left">
          <a href="/" className='logoButton'>
            <img src="https://upload.wikimedia.org/wikipedia/commons/b/b8/2021_Facebook_icon.svg" alt="foresbook" />
          </a>
          <div className="header-left-search">
            <SearchIcon />
            <input type="text" placeholder='Search Foresbook' />
          </div>
          <div className=" header-mid-tab icon3">
            <MenuIcon />
          </div>
        </div>
        <div className="header-mid">
          <a href="/">
            <div className="header-mid-tab header-mid-tab-active">
              <HomeIcon />
            </div>
          </a>
          <div className="header-mid-tab">
            <SmartDisplayIcon />
          </div>
          <div className="header-mid-tab">
            <StoreIcon />
          </div>
          <div className="header-mid-tab">
            <GroupIcon />
          </div>
          <div className="header-mid-tab icon1">
            <ViewComfyIcon />
          </div>
          <div className="header-mid-tab icon2">
            <MenuIcon />
          </div>
        </div>
        <div className="header-right">
          <div className="header-right-option icon4">
            <PlusIcon fontSize='medium' />
          </div>
          <div className="header-right-option icon5">
            <AppsIcon fontSize='medium' />
          </div>
          <div className="header-right-option">
            <SmsIcon fontSize='medium' />
          </div>
          <div className="header-right-option">
            <NotifIcon fontSize='medium' />
          </div>
          <div className="header-right-option" onClick={this.handleClickAccount} aria-expanded={this.state.accountDropdown ? 'true' : 'false'}>
            <AccountIcon fontSize='medium' />


          </div>
          <div className={`accdropdown ${this.state.accountDropdown ? 'show' : ''}`}>
            <a href={`/profile/${currentUser.id}`} style={{ textDecoration: 'none', color: 'black' }}>
              <div className="accdropdown-top">
                <Avatar />
                <h3>{currentUser.username}</h3>
              </div>
            </a>
            <div className="accdropdown-bottom">
              <div className='accdropdown-bottom-opt'>
                <div className="iconcircle">
                  <SettingIcon />
                </div>
                <h4>Setting & Privacy</h4>
              </div>
              <div className='accdropdown-bottom-opt'>
                <div className="iconcircle">
                  <QuestionIcon />
                </div>
                <h4>Help & Support</h4>
              </div>
              <div className='accdropdown-bottom-opt'>
                <div className="iconcircle">
                  <MoonIcon />
                </div>
                <h4>Display & Accessibility</h4>
              </div>
              <div className='accdropdown-bottom-opt'>
                <div className="iconcircle">
                  <ExcIcon />
                </div>
                <h4>Give Feedback</h4>
              </div>
              <a style={{ textDecoration: 'none', color: 'black' }} onClick={this.logOut}>
                <div className='accdropdown-bottom-opt'>
                  <div className="iconcircle">
                    <Logout />
                  </div>
                  <h4>Logout</h4>
                </div>
              </a>
            </div>
          </div>

        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(Header);
// export default Header