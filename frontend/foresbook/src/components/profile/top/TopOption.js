import React from 'react'
import './TopOption.css'

function TopOption({ title }) {
    if (title === 'Posts') {
        return (
            <div className='topOption topOption-active'>
                <h3>{title}</h3>
            </div>
        )
    }
    
    return (
        <div className='topOption'>
            <h3>{title}</h3>
        </div>
    )
}

export default TopOption