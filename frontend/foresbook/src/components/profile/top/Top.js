import { Avatar } from '@mui/material'
import './Top.css'
import PersonAddIcon from '@mui/icons-material/PersonAddAlt'
import ChatBubbleIcon from '@mui/icons-material/ChatBubble'
import MoreIcon from '@mui/icons-material/MoreHoriz'
import TopOption from './TopOption'
import UserService from '../../../services/user.services'

function Top(userViewed) { 
    // console.log(userViewed.userViewed.username)  
    return (
        <div className='top'>
            <div className="containerx">
                <div className="coverImage" style={{ backgroundImage: `url(https://images.freeimages.com/images/large-previews/ab7/gerber-and-rose-2-1544099.jpg)` }}>
                </div>
                <div className="container-info">
                    <div className='profilePicx'>
                        <Avatar className='profilePicAva' />
                    </div>
                    <div className="nameAndFriends">
                        <h1>{userViewed.userViewed.username}</h1>
                        <h3 style={{ color: 'gray' }}>0 friends</h3>
                    </div>
                    <div className="contactsButton">
                        <div className="cbutt addFriendButton">
                            <PersonAddIcon />
                            <h5>Add Friend</h5>
                        </div>
                        <div className="cbutt messageButton">
                            <ChatBubbleIcon />
                            <h5>Message</h5>
                        </div>
                    </div>
                </div>
                <div className="container-options">
                    <div className="container-options-left">
                        <div className="optionxx buttonHide6">
                            <TopOption title='Posts' />
                        </div>
                        <div className="optionxx buttonHide5">
                            <TopOption title='About' />
                        </div>
                        <div className="optionxx buttonHide4">
                            <TopOption title='Friends' />
                        </div>
                        <div className="optionxx buttonHide3">
                            <TopOption title='Photos' />
                        </div>
                        <div className="optionxx buttonHide2">
                            <TopOption title='Videos' />
                        </div>
                        <div className="optionxx buttonHide1">
                            <TopOption title='Check-ins' />
                        </div>
                        <TopOption title='More' />
                    </div>
                    <div className="container-options-right">
                        <div className="moreButton">
                            <MoreIcon fontSize='small' />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Top