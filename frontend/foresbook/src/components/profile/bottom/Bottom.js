import React, { useEffect, useState } from 'react'
import './Bottom.css'
import ClockIcon from '@mui/icons-material/WatchLater'
import FilterIcon from '@mui/icons-material/Tune'
import PostServices from '../../../services/post.services'
import FeedPost from '../../home/feed/FeedPost'

function Bottom(userViewed) {
    const today = new Date()
    const [userD, userM, userY] = userViewed.userViewed.dob.split("-")
    let age = today.getFullYear() - parseInt(userY)
    let m = today.getMonth() - parseInt(userM)
    if ((m < 0) || ((m === 0) && (today.getDate() < parseInt(userD)))) {
        age--
    }

    const [posts, setPosts] = useState([])

    useEffect(() => {
        getPosts()
    }, [])

    const getPosts = () => {
        PostServices.getByUserId(userViewed.userViewed.id)
            .then(res => {
                setPosts(res.data)
                // setIsLoading(false)
                // console.log(posts)
            })
    }

    const refreshPosts = () => {
        getPosts()
    }

    return (
        <div className='bottomx'>
            <div className="bottom-left">
                <div className="intro-box">
                    <div className="title">
                        <h2>Intro</h2>
                    </div>
                    <div className="intro-box-top">
                        <h3>Hello , my name is {userViewed.userViewed.username} !</h3>
                        <h3>{userViewed.userViewed.gender} ({age} years old)</h3>
                        <h3>Date of birth : {userViewed.userViewed.dob}</h3>

                    </div>
                    <div className="intro-box-bottom">
                        <ClockIcon />
                        <h4>Joined 11 January 2011</h4>
                    </div>
                </div>

                <div className="photos-box">
                    <div className="title">
                        <h2>Photos</h2>
                    </div>
                    <a href='' onClick={(e) => e.preventDefault()}>See all photos</a>
                </div>

                <div className="friends-box">
                    <div className="title">
                        <h2>Friends</h2>
                    </div>
                    <a href='' onClick={(e) => e.preventDefault()}>See all friends</a>
                </div>

            </div>

            <div className="bottom-right">
                <div className="posts-box">
                    <div className="title">
                        <h2>Posts</h2>
                    </div>
                    <div className="post-box-right">
                        <div className="filterButton">
                            <FilterIcon fontSize='small' />
                            <h4>Filter</h4>
                        </div>
                    </div>
                </div>

                {posts.slice(0).reverse().map(post => {
                    return (
                        <FeedPost
                            key={post.id}
                            postid={post.id}
                            userid={post.userId}
                            timestamp={post.createdAt}
                            message={post.content}
                            refreshPostFunc={refreshPosts}
                        />)
                })}

            </div>

        </div>

    )
}

export default Bottom