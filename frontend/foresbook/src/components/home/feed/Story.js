import { Avatar } from '@mui/material'
import React from 'react'
import './Story.css'

function Story({ storyPreviewImg, profilePicSrc, name }) {
    return (
        <div className='story' style={{backgroundImage:`url(${storyPreviewImg})`}}>
            <Avatar className='story-avatar'/>
            <h4>{name}</h4>
        </div>
    )
}

export default Story