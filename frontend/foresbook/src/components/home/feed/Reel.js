import React from 'react'
import './Reel.css'

function Reel({ reelPreviewImg }) {
    return (
        <div className='reel' style={{ backgroundImage: `url(${reelPreviewImg})` }}>

        </div>
    )
}

export default Reel