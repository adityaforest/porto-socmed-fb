import React, { useEffect, useState } from 'react'
import './Feed.css'
import FeedPost from './FeedPost'
import FeedPostInput from './FeedPostInput'
import StoryAndReel from './StoryAndReel'
import PostServices from '../../../services/post.services'

function Feed() {
  const [posts, setPosts] = useState([])
  const [isLoading, setIsLoading] = useState(true)

  useEffect(() => {
    getPosts()
  }, [])

  const getPosts = () => {
    PostServices.getAll()
      .then(res => {
        setPosts(res.data)
        setIsLoading(false)
        // console.log(posts)
      })
  }

  const refreshPosts = () => {
    getPosts()
  }

  if (isLoading) {
    return (
      <div>
        loading posts
      </div>
    )
  }
  return (
    <div className='feed'>
      <div className="wrapper-left">
      </div>
      <div className="wrapper-mid">
        <div className="wrapper-mid-sub">
          <StoryAndReel />
          <FeedPostInput refreshPostFunc={refreshPosts}/>
          <div className="posts">
            {posts.slice(0).reverse().map(post => {
              return (
                <FeedPost
                  key={post.id}
                  postid={post.id}
                  userid={post.userId}
                  timestamp={post.createdAt}
                  message={post.content}
                  refreshPostFunc={refreshPosts}
                />)
            })}
          </div>
        </div>
      </div>
      <div className="wrapper-right">
      </div>
    </div>
  )
}

export default Feed