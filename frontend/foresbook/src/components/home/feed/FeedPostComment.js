import { Avatar } from '@mui/material'
import React, { useEffect, useState } from 'react'
import moment from 'moment'
import { connect } from 'react-redux'

import DeleteIcon from '@mui/icons-material/Delete'
import UserServices from '../../../services/user.services'

import './FeedPostComment.css'
import CommentServices from '../../../services/comment.services'
import FeedPostCommentDelete from './FeedPostCommentDelete'

function FeedPostComment({ commentid, postuserid, userid, comment, timestamp, user: loggedinUser, refreshCommentFunc }) {

    const [userData, setUserData] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    const [showDelCon, setShowDelCon] = useState(false)

    useEffect(() => {
        getUserData()
    }, [])

    const getUserData = e => {
        UserServices.getById(userid)
            .then(res => {
                setUserData(res.data)
                setIsLoading(false)
            })
    }

    const onClickDelete = () => {
        CommentServices.deleteComment(commentid)
            .then(setShowDelCon(false))
            .then(refreshCommentFunc())
    }

    const setShowDelConxx = (x) => {
        setShowDelCon(x)
    }

    if (isLoading) {
        // console.log(`loggedinuserid: ${loggedinUser.id}`)
        return
    }

    return (
        <div className='feedPostComment'>
            <div className="comment-sub">
                <a href={`/profile/${userid}`}>
                    <Avatar sizes='small' />
                </a>
                <div className="comment-content">
                    <div className="comment-content-top">
                        <a href={`/profile/${userid}`} style={{ textDecoration: 'none', color: 'black', fontSize: '17px' }}>
                            <h5>{userData.username}</h5>
                        </a>
                        <div className="empty-space"></div>
                        <div className={(userid == loggedinUser.id || postuserid == loggedinUser.id) ? 'trashDiv' : 'invisible'}
                            onClick={(e) => setShowDelCon(true)}>
                            <DeleteIcon fontSize='12' />
                        </div>
                    </div>
                    <p>{comment}</p>
                </div>
            </div>
            <div className="comment-options">
                <p>Like</p>
                <p>Reply</p>
                <p>{moment(timestamp).fromNow()}</p>
            </div>

            <div className={showDelCon ? '' : 'invisible'}>
                <FeedPostCommentDelete setShowDelConx={setShowDelConxx} delfunc={onClickDelete} />
            </div>

        </div>
    )
}

function mapStateToProps(state) {
    const { user } = state.auth;
    return {
        user,
    };
}

export default connect(mapStateToProps)(FeedPostComment);
// export default FeedPostComment