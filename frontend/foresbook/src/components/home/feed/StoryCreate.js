import React from 'react'
import './StoryCreate.css'
import AddIcon from '@mui/icons-material/AddCircle'

function StoryCreate() {
    return (
        <div className='storyCreate'>
            <div className="bottom">
                <AddIcon className="addCircle" />
                <h4>Create Story</h4>
            </div>
        </div>
    )
}

export default StoryCreate