import { Avatar } from '@mui/material'
import React, { Component, useState } from 'react'
import { connect } from "react-redux";
import './FeedPostInput.css'
import VideoCamIcon from '@mui/icons-material/Videocam'
import PhotoLibraryIcon from '@mui/icons-material/PhotoLibrary'
import EmoticonIcon from '@mui/icons-material/InsertEmoticon'
import PostServices from '../../../services/post.services'

class FeedPostInput extends Component {
    constructor(props) {
        super(props)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.onContentChange = this.onContentChange.bind(this)

        this.state = {
            content: '',
            currentUser: this.props.user
        }
    }

    componentDidMount() {
        // console.log(this.state.currentUser)
    }

    onContentChange(e) {
        this.setState({
            content: e.target.value
        })
    }

    handleSubmit = (e) => {
        //no refresh
        e.preventDefault()
        // console.log(e)
        // console.log(this.state.currentUser.id)
        if (this.state.content.length > 0) {
            //send post
            PostServices.sendPost(this.state.content, this.state.currentUser.id)
            .then(console.log('post sended to database'))  
            .then(this.props.refreshPostFunc())          
        }

        //reset form input value
        this.setState({
            content: ''
        })

    }
    render() {
        return (
            <div className='feedPostInput'>
                <div className="feedPostInput-top">
                    <Avatar />
                    <form action="">
                        <input type="text" className='feedPostInput-top-input' placeholder={`What's on your mind`}
                            value={this.state.content} onChange={this.onContentChange} />
                        <button type='submit' onClick={this.handleSubmit} style={{ display: 'none' }}> Post</button>
                    </form>
                </div>
                <div className="feedPostInput-bottom">
                    <div className="feedPostInput-bottom-option">
                        <VideoCamIcon style={{ color: 'red' }} />
                        <h3>Live Video</h3>
                    </div>
                    <div className="feedPostInput-bottom-option">
                        <PhotoLibraryIcon style={{ color: 'green' }} />
                        <h3>Photo/Video</h3>
                    </div>
                    <div className="feedPostInput-bottom-option icon1">
                        <EmoticonIcon style={{ color: 'orange' }} />
                        <h3>Feeling/Activity</h3>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state.auth;
    return {
        user,
    };
}

export default connect(mapStateToProps)(FeedPostInput);
// export default FeedPostInput