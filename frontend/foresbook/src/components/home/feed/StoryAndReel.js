import React, { useRef } from 'react'
import './StoryAndReel.css'
import BookIcon from '@mui/icons-material/ImportContacts'
import MovieIcon from '@mui/icons-material/Movie'
import Story from './Story'
import ArrowLeftIcon from '@mui/icons-material/ArrowBackIos'
import ArrowRightIcon from '@mui/icons-material/ArrowForwardIos'
import StoryCreate from './StoryCreate'
import Reel from './Reel'
import ReelEnd from './ReelEnd'

function StoryAndReel() {
    const storyRef = useRef(null)
    const reelRef = useRef(null)
    const storyWrapper = useRef(null)
    const reelWrapper = useRef(null)
    const reelClick = () => {
        storyRef.current?.classList.remove('storyAndReel-top-option-active')
        reelRef.current?.classList.add('storyAndReel-top-option-active')
        storyWrapper.current?.classList.add('wrapper-inactive')
        reelWrapper.current?.classList.remove('wrapper-inactive')
    }
    const storyClick = () => {
        storyRef.current?.classList.add('storyAndReel-top-option-active')
        reelRef.current?.classList.remove('storyAndReel-top-option-active')
        storyWrapper.current?.classList.remove('wrapper-inactive')
        reelWrapper.current?.classList.add('wrapper-inactive')
    }

    const scollRef = useRef(null);
    const scroll = (scrollOffset) => {
        scollRef.current.scrollLeft += scrollOffset;
    }

    const leftRef = useRef(null)
    const rightRef = useRef(null)
    const onScroll = () => {
        // console.log(ref.current?.scrollLeft)  
        let maxScroll = scollRef.current?.scrollWidth - scollRef.current?.clientWidth
        // console.log(`max scroll is ${maxScroll}`)
        if (scollRef.current?.scrollLeft === 0) {
            leftRef.current?.classList.add('sbinactive')
            // console.log('mentok  kiribos')
        }
        else if (scollRef.current?.scrollLeft === maxScroll) {
            rightRef.current?.classList.add('sbinactive')
        }
        else {
            leftRef.current?.classList.remove('sbinactive')
            rightRef.current?.classList.remove('sbinactive')
        }
    }

    return (
        <div className='storyAndReel'>
            <div className="storyAndReel-top">
                <div className="storyAndReel-top-option storyAndReel-top-option-active" ref={storyRef} onClick={storyClick}>
                    <BookIcon />
                    <h4> Story </h4>
                </div>
                <div className="storyAndReel-top-option" ref={reelRef} onClick={reelClick}>
                    <MovieIcon />
                    <h4> Reels </h4>
                </div>
            </div>
            <div className="storyAndReel-bottom">
                <div className='slideButton sbleft sbinactive' onClick={() => scroll(-150)} ref={leftRef}>
                    <ArrowLeftIcon />
                </div>
                <div className='slideButton sbright' onClick={() => scroll(150)} ref={rightRef}>
                    <ArrowRightIcon />
                </div>
                <div className="">
                    <div className="container" ref={scollRef} onScroll={onScroll}>
                        <div className="wrapper" ref={storyWrapper}>
                            <StoryCreate />
                            <Story storyPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' name='username' />
                            <Story storyPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' name='username' />
                            <Story storyPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' name='username' />
                            <Story storyPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' name='username' />
                            <Story storyPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' name='username' />
                            <Story storyPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' name='username' />
                            <Story storyPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' name='username' />
                        </div>
                        <div className="wrapper wrapper-inactive" ref={reelWrapper}>
                            <Reel reelPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' />
                            <Reel reelPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' />
                            <Reel reelPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' />
                            <Reel reelPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' />
                            <Reel reelPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' />
                            <Reel reelPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' />
                            <Reel reelPreviewImg='https://cdn.pixabay.com/photo/2016/11/30/20/58/programming-1873854_960_720.png' />
                            <ReelEnd />
                        </div>
                    </div>
                </div>               
            </div>
        </div>
    )
}

export default StoryAndReel