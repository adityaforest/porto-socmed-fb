import React, { useEffect, useState } from 'react'
import './FeedPostDelete.css'
import XIcon from '@mui/icons-material/Close'

function FeedPostDelete({setShowDelConx,delfunc}) {
  // const [show,setShow] = useState(false)

  // const setShowx = (showxx) => {
  //   setShow(showxx)
  // }

  return (
    // <div className={show ? 'FeedPostCommentDelete' : 'invisible' }>
    <div className='feedPostDelete'>
      <div className="fpd-box" >
        <div className="fpd-closeicon" onClick={(e) => setShowDelConx(false)}>
        {/* <div className="fpcd-closeicon"> */}
          <XIcon />
        </div>
        <div className="fpd-box-top">
          <h3>Delete Post?</h3>
        </div>
        <div className="fpd-box-mid">
          <p>Are you sure want to delete this post ?</p>
        </div>
        <div className="fpd-box-bot">
          <div className="fpd-nobutt" onClick={(e) => setShowDelConx(false)}>
            <p>No</p>
          </div>
          <div className="fcd-delbutt" onClick={(e) => delfunc()}>
            <p>Delete</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default FeedPostDelete