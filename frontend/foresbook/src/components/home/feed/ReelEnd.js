import React from 'react'
import './ReelEnd.css'
import ArrowIcon from '@mui/icons-material/ArrowCircleRight'

function ReelEnd() {
  return (
    <div className='reelEnd'>
        <ArrowIcon fontSize='large' style={{color:'gray'}}/>
        <h4>See more reels</h4>
    </div>
  )
}

export default ReelEnd