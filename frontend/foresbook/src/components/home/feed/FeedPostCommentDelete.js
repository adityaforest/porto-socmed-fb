import React, { useEffect, useState } from 'react'
import './FeedPostCommentDelete.css'
import XIcon from '@mui/icons-material/Close'

function FeedPostCommentDelete({setShowDelConx,delfunc}) {
  // const [show,setShow] = useState(false)

  // const setShowx = (showxx) => {
  //   setShow(showxx)
  // }

  return (
    // <div className={show ? 'FeedPostCommentDelete' : 'invisible' }>
    <div className='feedPostCommentDelete'>
      <div className="fpcd-box" >
        <div className="fpcd-closeicon" onClick={(e) => setShowDelConx(false)}>
        {/* <div className="fpcd-closeicon"> */}
          <XIcon />
        </div>
        <div className="fpcd-box-top">
          <h3>Delete Comment?</h3>
        </div>
        <div className="fpcd-box-mid">
          <p>Are you sure want to delete this comment ?</p>
        </div>
        <div className="fpcd-box-bot">
          <div className="fpcd-nobutt" onClick={(e) => setShowDelConx(false)}>
            <p>No</p>
          </div>
          <div className="fcpd-delbutt" onClick={(e) => delfunc()}>
            <p>Delete</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default FeedPostCommentDelete