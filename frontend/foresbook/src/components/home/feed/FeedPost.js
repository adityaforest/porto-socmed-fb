import { Avatar, IconButton } from '@mui/material'
import React, { useEffect, useState } from 'react'
import moment from 'moment'
import './FeedPost.css'
import ThumbIcon from '@mui/icons-material/ThumbUp'
import ChatBubbleIcon from '@mui/icons-material/ChatBubbleOutline'
import ShareIcon from '@mui/icons-material/Share'
import FeedPostComment from './FeedPostComment'
import Emoji1 from '@mui/icons-material/Face2'
import Emoji2 from '@mui/icons-material/EmojiEmotions'
import Cam from '@mui/icons-material/PhotoCamera'
import Gif from '@mui/icons-material/GifBox'
import Sticker from '@mui/icons-material/Layers'
import UserServices from '../../../services/user.services'
import LikeServices from '../../../services/like.services'
import CommentServices from '../../../services/comment.services'
import PostServices from '../../../services/post.services'
import { connect } from 'react-redux'
import FeedPostDelete from './FeedPostDelete'
import DeleteIcon from '@mui/icons-material/Delete'


function FeedPost({ postid, userid, timestamp, message, user: loggedinUser , refreshPostFunc}) {

    const [userData, setUserData] = useState({})
    const [isLoading, setIsLoading] = useState(true)
    const [isLikedByMe, setIsLikedByMe] = useState(false)
    const [usersLiked, setUsersLiked] = useState([])

    const [commentInput, setCommentInput] = useState('')

    const [comments, setComments] = useState([])
    const [openAllComments, setOpenAllComments] = useState(false)

    const [showDelCon, setShowDelCon] = useState(false)


    const handleClickLike = e => {
        likeThisPost(e)
    }

    useEffect(() => {
        getUserData()
        // refreshLikes()
        getUsersLikedThisPost()
        isThisPostLikedByMe()
        refreshComments()
    }, [])

    const getUserData = e => {
        UserServices.getById(userid)
            .then(res => {
                setUserData(res.data)
                setIsLoading(false)
            })
    }

    const refreshLikes = () => {
        getUsersLikedThisPost()
        isThisPostLikedByMe()
    }

    const getUsersLikedThisPost = () => {
        LikeServices.getByPostId(postid)
            .then(res => {
                setUsersLiked(res.data)
            })
    }

    const isThisPostLikedByMe = () => {
        LikeServices.getByUserAndPostId(loggedinUser.id, postid)
            .then(res => {
                if (res.data) {
                    setIsLikedByMe(true)
                }
                else {
                    setIsLikedByMe(false)
                }
            })
    }

    const likeThisPost = e => {
        LikeServices.getByUserAndPostId(loggedinUser.id, postid)
            .then(res => {
                if (res.data) {
                    LikeServices.deleteByUserAndPostId(loggedinUser.id, postid)
                }
                else {
                    LikeServices.likeThisPost(postid, loggedinUser.id)
                }
                refreshLikes()
            })
    }

    const onChangeCommentInput = (e) => {
        setCommentInput(e.target.value)
    }

    const handleSubmitComment = (e) => {
        e.preventDefault()
        if (commentInput.length > 0) {
            //send comment            
            CommentServices.sendComment(loggedinUser.id, postid, commentInput)
                .then(res => {
                    setCommentInput('')
                    refreshComments()
                })
        }
    }

    const getAllComments = () => {
        CommentServices.getByPostId(postid)
            .then(res => {
                setComments(res.data)
            })
    }

    const refreshComments = () => {
        getAllComments()
    }

    const viewMoreCommentClick = (e) => {
        e.preventDefault()
        setOpenAllComments(true)
    }

    const onClickDeletex = () => {
        PostServices.deletePost(postid)
            .then(setShowDelCon(false))
            .then(refreshPostFunc())
    }

    const setShowDelConxx = (x) => {
        setShowDelCon(x)
    }

    if (isLoading) {
        // console.log(`loggedinuserid: ${loggedinUser.id}`)
        return
    }

    return (
        <div className='feedPost'>
            <div className={(userid == loggedinUser.id) ? 'trashDivx' : 'invisible'}
                onClick={(e) => setShowDelCon(true)}>
                <DeleteIcon fontSize='12' />
            </div>

            <div className="feedPost-top">
                <a href={`/profile/${userid}`}>
                    <Avatar className='feedPost-top-avatar' />
                </a>
                <div className="feedPost-top-info">
                    <a href={`/profile/${userid}`} style={{ textDecoration: 'none', color: 'black', fontSize: '13px' }}>
                        <h3>{userData.username}</h3>
                    </a>
                    <p>{moment(timestamp).fromNow()}</p>
                </div>
            </div>

            <div className="feedPost-bottom">
                <p>{message}</p>
            </div>

            <div className="feedPost-info">
                <div className="feedPost-info-left">
                    <div className={usersLiked.length ? "thumbIcon" : "invisible"} >
                        <ThumbIcon style={{ color: 'white', height: '15px', width: '15px' }} />
                    </div>
                    <p style={{ color: 'gray' }} className={usersLiked.length ? "" : "invisible"}>{usersLiked.length}</p>
                </div>
                <div className={comments.length ? "feedPost-info-right" : 'invisible'} >
                    <p style={{ color: 'gray' }}>{comments.length} comments</p>
                </div>
            </div>

            <div className="feedPost-options">
                <div className={isLikedByMe ? "feedPost-option feedPost-option-active" : "feedPost-option "} onClick={handleClickLike}>
                    <ThumbIcon />
                    <p>Like</p>
                </div>
                <div className="feedPost-option">
                    <ChatBubbleIcon />
                    <p>Comment</p>
                </div>
                <div className="feedPost-option">
                    <ShareIcon />
                    <p>Share</p>
                </div>
            </div>

            <div className="feedPost-comments">
                <a className={(comments.length - 3 > 0 && !openAllComments) ? 'viewMoreComment' : 'invisible'}
                    href=''
                    onClick={viewMoreCommentClick}>View {comments.length - 3} more comments</a>
                {
                    (!openAllComments && comments.length > 3) ? (
                        comments.slice(comments.length - 3, comments.length).map((comment) => {
                            return (
                                <FeedPostComment
                                    key={comment.id}
                                    commentid={comment.id}
                                    postuserid={userid}
                                    userid={comment.userId}
                                    comment={comment.content}
                                    timestamp={comment.createdAt}
                                    refreshCommentFunc={refreshComments} />
                            )
                        })
                    ) : (
                        comments.map((comment) => {
                            return (
                                <FeedPostComment
                                    key={comment.id}
                                    commentid={comment.id}
                                    postuserid={userid}
                                    userid={comment.userId}
                                    comment={comment.content}
                                    timestamp={comment.createdAt}
                                    refreshCommentFunc={refreshComments} />
                            )
                        })
                    )
                }
                <div className="commentInput-section">
                    <Avatar style={{ width: '30px', height: '30px' }} />
                    <form action="">

                        <div className="comment-input">
                            <input type="text" placeholder='Write a comment...' value={commentInput}
                                onChange={onChangeCommentInput} />
                            {/* <div className="emojis">
                                <IconButton className='embutton'>
                                    <Emoji1 fontSize='small' />
                                </IconButton>
                                <IconButton className='embutton'>
                                    <Emoji2 fontSize='small' />
                                </IconButton>
                                <IconButton className='embutton'>
                                    <Cam fontSize='small' />
                                </IconButton>
                                <IconButton className='embutton'>
                                    <Gif fontSize='small' />
                                </IconButton>
                                <IconButton className='embutton'>
                                    <Sticker fontSize='small' />
                                </IconButton>
                            </div> */}
                            <button type='submit' onClick={handleSubmitComment} style={{ display: 'none' }}> Post</button>
                        </div>
                    </form>
                </div>
            </div>

            <div className={showDelCon ? '' : 'invisible'}>
                <FeedPostDelete setShowDelConx={setShowDelConxx} delfunc={onClickDeletex} />
            </div>

        </div>
    )
}

function mapStateToProps(state) {
    const { user } = state.auth;
    return {
        user,
    };
}

export default connect(mapStateToProps)(FeedPost);

// export default FeedPost