import React from 'react'
import './Rightbar.css'
import VideoCallIcon from '@mui/icons-material/VideoCall'
import SearchIcon from '@mui/icons-material/Search'
import MoreIcon from '@mui/icons-material/MoreHoriz'
import { IconButton } from '@mui/material'
import OnlineUser from './OnlineUser'

function Rightbar() {
    return (
        <div className='rightbar'>
            <div className='rightbar-top'>
                <h4>Online Users</h4>
                <IconButton>
                    <VideoCallIcon />
                </IconButton>
                <IconButton>
                    <SearchIcon />
                </IconButton>
                <IconButton>
                    <MoreIcon />
                </IconButton>
            </div>
            <div className="rightbar-bottom">
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
                <OnlineUser username='username'/>
            </div>
        </div>
    )
}

export default Rightbar