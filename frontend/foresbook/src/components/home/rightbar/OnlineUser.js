import React from 'react'
import './OnlineUser.css'
import AccountIcon from '@mui/icons-material/AccountCircle'

function OnlineUser({ username }) {
    return (
        <div className='onlineUser'>
            <div className='profilePic' style={{ position: 'relative', width: '40px', height: '40px', display: 'flex', alignItems: 'end', justifyContent: 'end' }}>
                <AccountIcon fontSize='large' style={{ position: 'absolute', left: 0, top: 0, width: '100%', height: '100%', backgroundSize: 'cover' , color:'gray'}} />
                <div className='greenCircle' ></div>
            </div>
            <h4>{username}</h4>
        </div>
    )
}

export default OnlineUser