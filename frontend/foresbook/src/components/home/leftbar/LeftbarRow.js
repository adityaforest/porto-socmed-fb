import { Avatar } from '@mui/material'
import React from 'react'
import './LeftbarRow.css'

function LeftbarRow({src,Icon,title}) {
  return (
    <div className='leftbarRow'>
        {/* if props passed to this row is src , then create avatar Avatar component with src provided */}
        {src && <Avatar />}        
        {/* if props passed to this row is Icon , then create Icon component based on passed Icon component*/}
        {/* Don't forget if you pass component (ex. Icon) as a props , start with capital letter */}
        {Icon && <Icon />}
        {/* If passed props is Icon then rneder Icon else render Avatar */}
        {/* {Icon ? <Icon /> : <Avatar/>} */}
        {/* <Icon/> */}
        <h4>{title}</h4>
    </div>
  )
}

export default LeftbarRow