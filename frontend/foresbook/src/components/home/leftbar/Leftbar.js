import React, { Component } from 'react'
import './Leftbar.css'
import LeftbarRow from './LeftbarRow'
import HistoryIcon from '@mui/icons-material/History'
import PeopleIcon from '@mui/icons-material/People'
import GroupsIcon from '@mui/icons-material/Groups'
import StoreIcon from '@mui/icons-material/Storefront'
import VideoIcon from '@mui/icons-material/VideoLibrary'
import { ExpandMoreOutlined } from '@mui/icons-material'
import { connect } from 'react-redux'

class Leftbar extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { user: currentUser } = this.props;

    return (
      <div className='leftbar' >
        <a href={`/profile/${currentUser.id}`} style={{textDecoration:'none',color:'black'}}>
          <LeftbarRow title={currentUser.username} src='test' />
        </a>
        <LeftbarRow title='Friends' Icon={PeopleIcon} />
        <LeftbarRow title='Most Recent' Icon={HistoryIcon} />
        <LeftbarRow title='Groups' Icon={GroupsIcon} />
        <LeftbarRow title='Marketplace' Icon={StoreIcon} />
        <LeftbarRow title='Watch' Icon={VideoIcon} />
        <LeftbarRow title='See More' Icon={ExpandMoreOutlined} />
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(Leftbar);

// export default Leftbar