import axios from 'axios';
import authHeader from './auth-header';

// const API_URL = 'http://localhost:8080/api/v1/posts/';
const API_URL = `${process.env.REACT_APP_BE_URL}/api/v1/posts/`;

class PostService {  
  getAll() {
    return axios.get(API_URL , { headers: authHeader() });
  }

  getByUserId(userid) {
    return axios.get(API_URL + `user/${userid}`, { headers: authHeader() }); //put userid here
  }

  getByPostId() {
    return axios.get(API_URL + 'postid', { headers: authHeader() }); //put postid here
  }

  sendPost(content,userid){
    return axios.post(API_URL , {content:content,userId:userid},{ headers: authHeader() })
  }

  deletePost(postid){
    return axios.delete(API_URL + postid , { headers: authHeader() })
  }

}

export default new PostService();