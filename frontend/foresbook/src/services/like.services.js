import axios from 'axios';
import authHeader from './auth-header';

// const API_URL = 'http://localhost:8080/api/v1/likes/';
const API_URL = `${process.env.REACT_APP_BE_URL}/api/v1/likes/`;

class LikeService {  
  getByUserAndPostId(userid,postid) {
    return axios.get(API_URL + `?userid=${userid}&postid=${postid}`, { headers: authHeader() }); //put user and post id here
  }

  deleteByUserAndPostId(userid,postid){
    return axios.delete(API_URL + `?userid=${userid}&postid=${postid}`, { headers: authHeader() }); //put user and post id here
  }

  getByUserId() {
    return axios.get(API_URL + 'user/userid', { headers: authHeader() }); //put userid here
  }

  getByPostId(postid) {
    return axios.get(API_URL + `post/${postid}`, { headers: authHeader() }); //put postid here
  }

  likeThisPost(postid,userid){
    return axios.post(API_URL , {userId:userid,postId:postid}, { headers: authHeader() })
  }



}

export default new LikeService();