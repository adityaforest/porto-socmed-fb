import axios from 'axios';
import authHeader from './auth-header';

// const API_URL = 'http://localhost:8080/api/v1/comments/';
const API_URL = `${process.env.REACT_APP_BE_URL}/api/v1/comments/`;

class CommentService {  

  getByPostId(postid) {
    return axios.get(API_URL + `post/${postid}`, { headers: authHeader() }); //put postid here
  }

  getByUserId() {
    return axios.get(API_URL + 'user/userid', { headers: authHeader() }); //put userid here
  }

  sendComment(userid,postid,content){
    return axios.post(API_URL , {userId:userid,postId:postid,content:content} , { headers: authHeader() })
  }

  deleteComment(commentid){
    return axios.delete(API_URL + commentid , { headers: authHeader() })
  }
}

export default new CommentService();