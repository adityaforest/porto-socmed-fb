import axios from 'axios';
import authHeader from './auth-header';

// const API_URL = 'http://localhost:8080/api/v1/users/';
const API_URL = `${process.env.REACT_APP_BE_URL}/api/v1/users/`;

class UserService {    
    getAll() {
        return axios.get(API_URL , { headers: authHeader() }); 
    }

    getById(userid) {
        return axios.get(API_URL + userid, { headers: authHeader() }); //put userid here
    }        

}

export default new UserService();