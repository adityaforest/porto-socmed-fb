import React, { Component } from 'react'
import './Login.css'
import { DropdownDate, DropdownComponent } from 'react-dropdown-date'
import QuestionIcon from '@mui/icons-material/QuestionMarkRounded'
import CloseIcon from '@mui/icons-material/Close'

import { redirect } from 'react-router-dom';
import { Navigate } from 'react-router-dom'
// import { useNavigate } from "react-router-dom";

import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import CheckButton from "react-validation/build/button";
import { isEmail } from "validator";

import { connect } from "react-redux";
import { login, register } from "./actions/auth";

import AuthService from "./services/auth.service";

// let navigate = useNavigate();

const formatDate = (date) => {	// formats a JS date to 'yyyy-mm-dd'
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;

  return [year, month, day].join('-');
}

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        <p style={{ color: 'red' }}>This field is required!</p>
      </div>
    );
  }
}

const vusername = (value) => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger" role="alert">
        <p style={{ color: 'red' }}>The username must be between 3 and 20 characters.</p>
      </div>
    );
  }
};

const vpassword = (value) => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger" role="alert">
        <p style={{ color: 'red' }}>The password must be between 6 and 40 characters.</p>
      </div>
    );
  }
};

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleLogin = this.handleLogin.bind(this);
    this.onChangeLog_Username = this.onChangeLog_Username.bind(this);
    this.onChangeLog_Password = this.onChangeLog_Password.bind(this);

    this.handleRegister = this.handleRegister.bind(this);
    this.onChangeReg_Username = this.onChangeReg_Username.bind(this);
    this.onChangeReg_Password = this.onChangeReg_Password.bind(this);
    this.vconpassword = this.vconpassword.bind(this);
    this.vusernameExist = this.vusernameExist.bind(this);

    this.state = {
      log_Username: "",
      log_Password: "",
      loading: false,

      reg_Show: false,
      reg_Username: '',
      reg_Password: '',
      reg_DOB: '2012-11-01',
      reg_Gender: 'female',
      successful: false,

      usernameList: []
    };
  }

  async componentDidMount() {
    await AuthService.getAllUsername()
      .then(resx => {
        resx.data.forEach(element => {
          this.state.usernameList.push(element)
        });
      })
    // .finally(console.log(this.state.usernameList))        
  }

  onChangeLog_Username(e) {
    this.setState({
      log_Username: e.target.value
    })
  }

  onChangeLog_Password(e) {
    this.setState({
      log_Password: e.target.value
    })
  }

  onChangesReg_Show(state) {
    this.setState({
      reg_Show: state,
    })
  }

  onChangeReg_Username(e) {
    this.setState({
      reg_Username: e.target.value,
    });

  }

  onChangeReg_Password(e) {
    this.setState({
      reg_Password: e.target.value,
    });
  }

  vconpassword(value) {
    if (value != this.state.reg_Password) {
      return (
        <div className="alert alert-danger" role="alert">
          <p style={{ color: 'red' }}>Confirm password not match</p>
        </div>
      );
    }
  }

  vusernameExist(value) {
    for (let i in this.state.usernameList) {
      let x = this.state.usernameList[i].username
      if (x == value) {
        return (
          <div className="alert alert-danger" role="alert">
            <p style={{ color: 'red' }}>Username already exist</p>
          </div>
        )
      }
    }
  }

  onChangeReg_DOB(newDate) {
    this.setState({
      reg_DOB: newDate
    })
  }

  onChangeReg_Gender(newGender) {
    this.setState({
      reg_Gender: newGender,
    })
  }

  handleLogin(e) {
    e.preventDefault();

    this.setState({
      loading: true,
    });

    this.form.validateAll();

    const { dispatch, history } = this.props;

    if (this.checkBtnLog.context._errors.length === 0) {
      dispatch(login(this.state.log_Username, this.state.log_Password))
        .then(() => {
          history.push("/");
          window.location.reload();
        })
        .catch(() => {
          this.setState({
            loading: false
          });
        });
    } else {
      this.setState({
        loading: false,
      });
    }
  }

  handleRegister(e) {
    e.preventDefault();

    // console.log(this.state)
    // this.setState({
    //   successful: false,
    // });

    this.form.validateAll();

    // if (this.checkBtnReg.context._errors.length === 0) {
    //   this.props
    //     .dispatch(
    //       register(this.state.reg_Username, this.state.reg_Password, this.state.reg_DOB, this.state.reg_Gender)
    //     )
    //     .then(() => {
    //       this.setState({
    //         successful: true,
    //       });
    //     })
    //     .catch(() => {
    //       this.setState({
    //         successful: false,
    //       });
    //     });
    // }
  }

  render() {
    const { isLoggedIn, message } = this.props;
    console.log('login = ' + isLoggedIn)
    if (isLoggedIn) {
      // return useNavigate().navigate("/");
      // return redirect('/')
      return <Navigate replace to="/" />
    }

    return (
      <div className='login'>
        <div className="login-left">
          <div className="login-left-content">
            <div className="logoForesbook">
            </div>
            <p>FB Clone made by Aditya Forest</p>
            <p>Stack = FE: React + Redux + Material UI , BE: Express + JWT + Postgresql + Sequelize</p>
            <h4>This website is created for my portofolio , so use it just for fun ! Don't use / post any actual or private information on this site !</h4>
          </div>

        </div>
        <div className="login-right">
          <div className="login-right-box">

            <Form
              onSubmit={this.handleLogin}
              ref={(c) => {
                this.form = c;
              }}
            >
              <div className="login-right-top">
                <div className="form-group lfg">
                  <Input placeholder='Username'
                    type="text"
                    className="form-control lfgInput"
                    name="logusername"
                    value={this.state.log_Username}
                    onChange={this.onChangeLog_Username}
                    validations={[required]}
                  />
                </div>
                <div className="form-group lfg">
                  <Input placeholder='Password'
                    type="password"
                    className="form-control lfgInput"
                    name="logpassword"
                    value={this.state.log_Password}
                    onChange={this.onChangeLog_Password}
                    validations={[required]}
                  />
                </div>
                <button className='loginButton form-group lfg' >
                  <h3>Log in</h3>
                </button>
                <a href="">Forgotten password?</a>
              </div>

              {message && (
                <div className="form-group">
                  <div className="alert alert-danger" role="alert">
                    {message}
                  </div>
                </div>
              )}
              <CheckButton
                style={{ display: "none" }}
                ref={(c) => {
                  this.checkBtnLog = c;
                }}
              />

            </Form>
            <div className="login-right-bottom">
              <div className="login-right-bottom-sub">
                <div className='createNewAccButton' onClick={() => this.onChangesReg_Show(true)}>
                  <h3>Create New Account</h3>
                </div>
              </div>
            </div>
          </div>
          <div className="login-right-veryBottom">
            <a href="">Create a page </a>
            <h5>for a celebrity , brand , or business.</h5>
          </div>
        </div>

        {/* REGISTER SECTION */}
        <div className={`register-static ${this.state.reg_Show ? 'register-show' : ''}`}>
          <div className="register-box">
            <div className="closeRegIcon" onClick={() => this.onChangesReg_Show(false)}>
              <CloseIcon fontSize='large' />
            </div>
            <div className="register-title">
              <h1>Sign Up</h1>
              <p>It's quick and easy.</p>
            </div>
            <Form
              onSubmit={this.handleRegister}
              ref={(c) => {
                this.form = c;
              }}
            >
              <div className="register-body">
                <div className="form-group rfg">
                  <Input placeholder='Username'
                    type="text"
                    className="form-control rfgInput"
                    name="username"
                    value={this.state.reg_Username}
                    onChange={this.onChangeReg_Username}
                    validations={[required, vusername, this.vusernameExist]}
                  />
                </div>
                <div className="form-group rfg">
                  <Input placeholder='Password'
                    type="password"
                    className="form-control rfgInput"
                    name="password"
                    value={this.state.reg_Password}
                    onChange={this.onChangeReg_Password}
                    validations={[required, vpassword]} />
                </div>

                <div className="form-group rfg">
                  <Input placeholder='Confirm Password'
                    type="password"
                    className="form-control rfgInput"
                    name="conpassword"
                    // value={this.state.reg_Password}
                    // onChange={this.onChangeReg_Conpassword}
                    validations={[required, this.vconpassword]} />
                </div>

                <div className="dateLabel">
                  <p>Date of birth</p>
                  <div className="iconCirclex">
                    <QuestionIcon />
                  </div>
                </div>

                <div className="dateConClass">
                  <div className="yearConClass">
                  </div>
                  <div className="monConClass">
                  </div>
                  <div className="dayConClass">
                  </div>
                </div>
                <DropdownDate
                  startDate={'1905-01-01'}
                  selectedDate={this.state.reg_DOB}
                  order={[
                    DropdownComponent.day,
                    DropdownComponent.month,
                    DropdownComponent.year
                  ]}
                  onDateChange={(date) => {
                    console.log(date);
                    this.onChangeReg_DOB(formatDate(date))
                  }}
                  classes={
                    {
                      dateContainer: 'dateConClass',
                      yearContainer: 'yearConClass',
                      monthContainer: 'monConClass',
                      dayContainer: 'dayConClass'
                    }
                  }
                />

                <div className="dateLabel">
                  <p>Gender</p>
                  <div className="iconCirclex">
                    <QuestionIcon />
                  </div>
                </div>

                <div className="genderOptions">
                  <div className="genderOption">
                    <label htmlFor="female">Female</label>
                    <input type="radio" id="female" checked={'female' === this.state.reg_Gender}
                      onChange={(e) => this.onChangeReg_Gender('female')} />
                  </div>
                  <div className="genderOption">
                    <label htmlFor="male">Male</label>
                    <input type="radio" id="male" checked={'male' === this.state.reg_Gender}
                      onChange={(e) => this.onChangeReg_Gender('male')} />
                  </div>
                  <div className="genderOption">
                    <label htmlFor="custom">Custom</label>
                    <input type="radio" id="custom" checked={'custom' === this.state.reg_Gender}
                      onChange={(e) => this.onChangeReg_Gender('custom')} />
                  </div>
                </div>

                <div className="bottomBlabla">
                  <p>People who use our service may have uploaded your contact information to Foresbook. Learn more.</p>
                  <p>By clicking Sign Up, you agree to our Terms, Privacy Policy and Cookies Policy. You may receive SMS notifications from us and can opt out at any time.</p>
                </div>

                <button className="signupButton form-group rfg">
                  <h3>Sign Up</h3>
                </button>

              </div>

              {message && (
                <div className="form-group">
                  <div className={this.state.successful ? "alert alert-success" : "alert alert-danger"} role="alert">
                    {message}
                  </div>
                </div>
              )}
              <CheckButton
                style={{ display: "none" }}
                ref={(c) => {
                  this.checkBtnReg = c;
                }}
              />
            </Form>
          </div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { isLoggedIn } = state.auth;
  const { message } = state.message;
  return {
    isLoggedIn,
    message,
  };
}

export default connect(mapStateToProps)(Login)
// export default Login

// export default () => {
//   return {Login , connect(mapStateToProps)(Login)}
// }