import React, { Component } from "react";
import { connect } from "react-redux";

import './App.css';
import HomePage from './HomePage';
import ProfilePage from './ProfilePage'
import Login from './Login';
import {
  BrowserRouter as Router,
  // Switch,
  Routes,
  Route,
  Link
} from "react-router-dom";

import { history } from './helpers/history';
import { clearMessage } from "./actions/message";

class App extends Component {
  constructor(props) {
    super(props)    

    this.state = {
      currentUser: undefined,
    };

    history.listen((location) => {
      props.dispatch(clearMessage()); // clear message when changing location
    });

  }

  componentDidMount() {
    const user = this.props.user;

    if (user) {
      this.setState({
        currentUser: user,    
      });
    }
  }

  render() {
    const { currentUser } = this.state;
    return (
      <div className="app">
        <Router history={history}>
          <Routes>
            <Route exact path="/" element={<HomePage />} />
            <Route exact path="/profile/:userid" element={<ProfilePage />} />
            <Route exact path="/login" element={<Login />} />
          </Routes>
        </Router>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state.auth;
  return {
    user,
  };
}

export default connect(mapStateToProps)(App);

// export default App;
