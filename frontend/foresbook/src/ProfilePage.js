import React, { useEffect, useLayoutEffect } from 'react'
import './ProfilePage.css'
import Top from './components/profile/top/Top'
import Bottom from './components/profile/bottom/Bottom'
import { useState } from 'react'
import MoreIcon from '@mui/icons-material/MoreHoriz'
import { Avatar } from '@mui/material'
import Header from './components/header/Header';
import { useParams } from 'react-router-dom'
import userServices from './services/user.services'

function ProfilePage() {
  const [fix, setFix] = useState(false)
  const [userViewed, setUserViewed] = useState({})
  const [isLoading, setIsLoading] = useState(true)
  
  const setFixed = () => {
    if (window.scrollY >= 410) {
      setFix(true)
    }
    else {
      setFix(false)
    }
  }
  
  window.addEventListener('scroll', setFixed)

  useEffect(() => {
    fetchData()
  }, [])

  const param = useParams()
  const fetchData = () => {
    userServices.getById(param.userid)
    .then(res => {
      setUserViewed(res.data)
      setIsLoading(false)
    })
  }
  
  if (isLoading) {
    return <div className="profilePage">Loading fetching user data...</div>;
  }
  return (    
    <div className='profilePage'>
      <Header />
      <div className="profilePageBody">
        <div className={fix ? 'subnavShow' : "subnavHide"}>
          <div className="containerxx">
            <div className="container-leftx">
              <Avatar fontSize='small' />
              <h3>{userViewed.username}</h3>
            </div>
            <div className="container-rightx">
              <div className="moreButton">
                <MoreIcon fontSize='small' />
              </div>
            </div>
          </div>
        </div>
        <Top userViewed={userViewed} />
        <Bottom userViewed={userViewed} />
      </div>
    </div>
  )
}

export default ProfilePage