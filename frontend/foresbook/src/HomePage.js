import React, { Component } from 'react'
import { connect } from "react-redux";
import { Navigate } from 'react-router-dom'

import './HomePage.css'
import Leftbar from './components/home/leftbar/Leftbar'
import Feed from './components/home/feed/Feed';
import Rightbar from './components/home/rightbar/Rightbar';
import Header from './components/header/Header';

class HomePage extends Component {

    render() {
        const { user: currentUser } = this.props;
        // console.log(currentUser)
        if (!currentUser) {
            // return <Redirect to="/login" />;
            return <Navigate replace to="/login" />
        }

        return (
            <div className='homePage'>
                <Header />
                <div className="homeBody">
                    <Leftbar />
                    <Feed />
                    <Rightbar />
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { user } = state.auth;
    return {
        user,
    };
}

export default connect(mapStateToProps)(HomePage);
// export default HomePage